#pragma once
#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include "GetConfig.h"
using namespace std;
using namespace cv;

struct Line
{
	float k;
	float b;
	bool isVertical = false;	
};

class BlockDC
{
public:
	BlockDC();
	BlockDC(const unsigned int& numX, const unsigned int& numY, const vector<Point2f>& ver) :\
		dnb_numX(numX), dnb_numY(numY), vertices(ver){};
	~BlockDC();	 
	int calBlock();
	int initBlock(const unsigned int& numX, const unsigned int& numY, const vector<Point2f>& ver);
	int initBlock();
	//find good point's nearest neigbor on trackcross-gridding, output nearest neigbor's coordinate and dist btw goodpoint and neigbor.
	int calAddress(const cv::Point2i& pointOnTempl, cv::Point2f& pointOnImg);
	int findNN(const Point2f& point, Point2f& nnCoordinate, float& rms);

public:
	float getDNBStepX();
	float getDNBStepY();
	Line topHorLine, leftVerLine;
	vector<Point2f> vertices;  // 4 vertex , P0-P4 clockwise
	static int initPitchsize(const string& strChipPitch, const string& imgScale);
	static float pitchSize;
	friend class BlockForAddress;

private:
	unsigned int dnb_numX;
	unsigned int dnb_numY;	
	float dnb_stepX;
	float dnb_stepY;	
	int calStep();
	int calLineSlope();
	//good point in block's address in one direction.
	float getAddress(const Point2f& point, const Line& line, const float dnb_step);
};


