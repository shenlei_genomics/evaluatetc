#pragma once
#include "SelectImages.h"
#include "SelectGoodPoints.h"
#include "BlockDC.h"
#include "BlockForAddress.h"
#include <limits>
#include <iostream>
//#define _DEBUG



int getFiles(const std::string& path, const std::string& ext, std::vector<std::string>& files)
{
	intptr_t file_handle = 0;
	struct _finddata_t file_info;
	std::string path_name, ext_name;
	if (strcmp(ext.c_str(), "") != 0)//if not empty
		ext_name = "\\*" + ext;
	else
		ext_name = "\\*";

	if ((file_handle = _findfirst(path_name.assign(path).append(ext_name).c_str(), &file_info)) != -1)
	{
		do
		{
			if (file_info.attrib & _A_SUBDIR)
			{
				if (strcmp(file_info.name, ".") != 0 && strcmp(file_info.name, "..") != 0)
				{
					getFiles(path_name.assign(path).append("\\").append(file_info.name), ext, files);
				}
			}
			else
			{
				if (strcmp(file_info.name, ".") != 0 && strcmp(file_info.name, "..") != 0)
				{
					files.push_back(path_name.assign(path).append("\\").append(file_info.name));
					//files.push_back(file_info.name);
				}
			}
		} while (_findnext(file_handle, &file_info) == 0);
		_findclose(file_handle);
	}
	return 0;
}

void Addlist(vector<IMGINFO>& imginfolist, const IMGINFO& info, const size_t& TIF_COUNT)
{
	imginfolist.push_back(info);
	if (imginfolist.size() < TIF_COUNT + 1)
	{		
		return;
	}
	sort(imginfolist.begin(), imginfolist.end(), [](const IMGINFO& info1, const IMGINFO& info2)->bool {return info1.rms4sel < info2.rms4sel; });
	imginfolist.pop_back();
}

int getFile(const std::string& fn, std::vector<std::string>& file_names)
{
	ifstream fs(fn);
	cout << fn << endl;
	std::string oneFn;
	while(fs)
	{
		fs >> oneFn;
		file_names.push_back(oneFn);
	}	
	return 0;
}

int initBlocksForAddress(vector<BlockForAddress> &blocks, const vector<Point2f> &trackcross, const size_t& BLOCKROW, const size_t& BLOCKCOL)
{
	for (size_t row = 0; row < BLOCKROW; row++)
	{
		for (size_t col = 0; col < BLOCKCOL; col++)
		{
			vector<Point2f> vers(4);
			size_t topleftVerIndex = row * (BLOCKCOL + 1) + col;
			//P0-P4  clockwise
			vers[0] = trackcross[topleftVerIndex]; vers[1] = trackcross[topleftVerIndex + 1];
			vers[2] = trackcross[topleftVerIndex + 1 + BLOCKCOL + 1]; vers[3] = trackcross[topleftVerIndex + BLOCKCOL + 1];
			blocks.emplace_back(col, row, vers);
			if (blocks[row*BLOCKCOL + col].initBlockForAddress() < 0)
			{
				cerr << "Error:calculate block info wrong! Block id:" << row*BLOCKCOL + col << endl;
				return -1;
			}
		}
	}
	return 0;
}

int ProcessOneBlock(const Mat &goodPointsMask, const Mat(&centroid)[2], const bool if_outside_blk,BlockForAddress &block, IMGINFO& info)
{	
	//cout << if_outside_blk <<"\t";
	if (!goodPointsMask.empty())
	{
		auto MAX_INT = [](const float x, const float y)->int {return max(static_cast<int>(x), static_cast<int>(y)); };
		auto MIN_INT = [](const float x, const float y)->int {return min(static_cast<int>(x), static_cast<int>(y)); };
		const vector<Point2f>& vertices = block.getVertex();
		if (vertices.empty() || vertices.size() < 4)
		{
			cerr << "Vertex num is not 4!" << endl;
			return -1;
		}
		//topleft, topright, bottom left, bottom right point
		const Point2f &tl = vertices[0], &tr = vertices[1], &bl = vertices[3], &br = vertices[2];
		float block_rms = 0.0f, tmp_rms4sel = 0.0f;
		//int n = 0;
		vector<Point2f> p4cal_img, p4cal_templ;		
		for (int row = MAX_INT(max(tl.y, tr.y) + 4, 0); row < MIN_INT(min(bl.y, br.y) - 2, goodPointsMask.rows); row++)
		{
			const uchar* gpm = goodPointsMask.ptr<uchar>(row);
			for (int col = MAX_INT(max(tl.x, bl.x) + 4, 0); col < MIN_INT(min(tr.x, br.x) - 2, goodPointsMask.cols); col++)
			{
				if (gpm[col] == 255)
				{
					Point2f tmpPoint(static_cast<float>(col), static_cast<float>(row));
					Point2f centroid_offset(centroid[0].at<float>(tmpPoint), centroid[1].at<float>(tmpPoint));
					tmpPoint.x = tmpPoint.x + centroid_offset.x;
					tmpPoint.y = tmpPoint.y + centroid_offset.y;
					Point2f nn,p_tc;
					float rms = 0.0f;
					//if (!block.addressOnTempl(tmpPoint, nn, rms))
					if (!block.addressOnTempl(tmpPoint, nn, p_tc, rms))
					{
#ifdef _DEBUG
						
						ofstream fs("./goodpoints_blk.txt", iostream::app);
						if (fs)
						{
							fs << tmpPoint.x << "\t" << tmpPoint.y << "\n";

						}
						fs.close();
						ofstream fs2("./goodpoints_tc.txt", iostream::app);
						if (fs2)
						{
							fs2 << p_tc.x << "\t" << p_tc.y << "\n";

						}
						fs2.close();					

#endif // _DEBUG

						if (if_outside_blk)
						{							
							info.points4comp_img.push_back(tmpPoint);
							info.points4comp_templ.push_back(nn);
							info.points4comp_tc.push_back(p_tc);
							info.rms4comp += rms;
						}					
						
						if (rms < RMSTH)
						{
							//p4cal_img.push_back(tmpPoint);
							//p4cal_templ.push_back(nn);							
							info.points4cal_img.push_back(tmpPoint);
							info.points4cal_templ.push_back(nn);
						}

						info.rms4sel += rms;
						++info.n4sel;
					}
				}
			}
		}
		
		/*cv::RNG rng(888);		
		if (p4cal_img.size() > COUNTTH)
		{
			float part = static_cast<float>(COUNTTH) / static_cast<float>(p4cal_img.size());
			for (auto it1 = p4cal_img.cbegin(), it2 = p4cal_templ.cbegin(); it1 != p4cal_img.cend(), it2 != p4cal_templ.cend(); ++it1, ++it2)
			{
				double rn = rng.uniform(0.0f, 1.0f);
				if (rn < part)
				{
					info.points4cal_img.push_back(*it1);
					info.points4cal_templ.push_back(*it2);					
				}				
			}
		}
		else
		{
			for (auto it1 = p4cal_img.cbegin(), it2 = p4cal_templ.cbegin(); it1 != p4cal_img.cend(), it2 != p4cal_templ.cend(); ++it1, ++it2)
			{
				info.points4cal_img.push_back(*it1);
				info.points4cal_templ.push_back(*it2);
			}
		}*/
		return 0;
	}
	else
	{
		return -1;
	}
}




