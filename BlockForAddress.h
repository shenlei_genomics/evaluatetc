#pragma once
#include "BlockDC.h"
#include "GetConfig.h"


//const unsigned int BLOCKROW = 10;
//const unsigned int BLOCKCOL = 10;

class BlockForAddress
{
public:
	BlockForAddress();	
	BlockForAddress(const unsigned int &xindex, const unsigned int &yindex, const std::vector<cv::Point2f> &ver) \
		:startX(xindex), startY(yindex), m_block(BlockDC(DNBNUMX[xindex], DNBNUMY[yindex], ver)){};
	~BlockForAddress();
	int initBlockForAddress();
	const std::vector<cv::Point2f>& getVertex() const { return m_block.vertices; };
	int addressOnTempl(const cv::Point2f &point, cv::Point2f &pointOnTempl, float &rms);
	int addressOnTempl(const cv::Point2f &point, cv::Point2f &pointOnTempl, cv::Point2f &point_tc, float &rms);
	static inline size_t getBlockrow() { return DNBNUMY.size(); }
	static inline size_t getBlockcol() { return DNBNUMX.size(); }
	static inline const std::vector<int>& getDnbnumX(){ return DNBNUMX; };
	static inline const std::vector<int>& getDnbnumY(){ return DNBNUMY; };
	static int initTemplate(const std::string& strDnbnumX, const std::string& strDnbnumY, const float imgScale);
	static int initTemplate(const std::string& strDnbnumX, const std::string& strDnbnumY, const float pitchSize, const bool& is_extend);
private:
	unsigned int startX;
	unsigned int startY;
	float dnb_num_start_X;
	float dnb_num_start_Y;
public:
	static std::vector<int> DNBNUMX;
	static std::vector<int> DNBNUMY;
	
	static std::vector<float> START_ADDRESS_X;
	static std::vector<float> START_ADDRESS_Y;
	BlockDC m_block;
};

