#include "BlockDC.h"
#include <math.h>
#include <limits>

BlockDC::BlockDC()
{
}


BlockDC::~BlockDC()
{
	this->vertices.clear();
}

float BlockDC::getDNBStepX()
{
	return this->dnb_stepX;
}

float BlockDC::getDNBStepY()
{
	return this->dnb_stepY;
}

float BlockDC::getAddress(const Point2f& point, const Line& line, const float dnb_step)
{
	auto calDist = [point](float k, float b)->float {return abs(k*point.x - point.y + b) / sqrt(k*k + 1); };

	float tmpDist = 0.0f;
	if (line.isVertical)
	{
		tmpDist = abs(point.x - this->vertices[0].x);
	}
	else
	{
		tmpDist = calDist(line.k, line.b);
	}
	return tmpDist / dnb_step;
}

int BlockDC::findNN(const Point2f& point, Point2f& nnCoordinate, float& rms)
{
	float yAddress = this->getAddress(point, this->topHorLine, this->dnb_stepY);
	float xAddress = this->getAddress(point, this->leftVerLine, this->dnb_stepX);
	vector<Point2i> neigbors = { Point2i(static_cast<int>(xAddress-1), static_cast<int>(yAddress-1)), \
		                         Point2i(static_cast<int>(xAddress-1), static_cast<int>(yAddress)),\
		                         Point2i(static_cast<int>(xAddress - 1), static_cast<int>(yAddress+1)), \
		                         Point2i(static_cast<int>(xAddress), static_cast<int>(yAddress-1)),\
		                         Point2i(static_cast<int>(xAddress), static_cast<int>(yAddress)), \
		                         Point2i(static_cast<int>(xAddress), static_cast<int>(yAddress+1)), \
		                         Point2i(static_cast<int>(xAddress + 1), static_cast<int>(yAddress-1)), \
		                         Point2i(static_cast<int>(xAddress+1), static_cast<int>(yAddress)), \
		                         Point2i(static_cast<int>(xAddress + 1), static_cast<int>(yAddress + 1)) };
	
	auto f = [](Point2f p1, Point2f p2) -> float {return sqrt((p2.x - p1.x)*(p2.x - p1.x) + (p2.y - p1.y)*(p2.y - p1.y)); };

	rms = (numeric_limits<float>::max)();
	for(auto itn = neigbors.cbegin(); itn != neigbors.cend(); ++itn)
	{
		Point2f pointOnImg;
		this->calAddress(*itn, pointOnImg);
		float d = f(pointOnImg, point);
//#ifdef _DEBUG
//            cout << "d in func:findNN ="<<d<<endl;
//#endif
			if (rms > d)
			{
				nnCoordinate = pointOnImg;
				rms = d;
//#ifdef _DEBUG
//            cout << "rms in func:findNN ="<<rms<<endl;
//#endif
			}			
		
	}
	if (rms == (numeric_limits<float>::max)())
	{
		return -2;
	}
	return 0;
}


int BlockDC::calBlock()
{
	int ret = this->calStep();
	if (!ret)
	{
		ret = this->calLineSlope();
	}
	return ret;	
}

float BlockDC::pitchSize ;
int BlockDC::initPitchsize(const string& strChipPitch, const string& strImgScale)
{
	if (stof(strChipPitch) == 0)
	{
		return -1;
	}
	pitchSize = stof(strChipPitch) / stof(strImgScale);
	return 0;
}

int BlockDC::calStep()
{
	if (this->vertices.empty() || this->vertices.size() != 4)
	{
		std::cerr << "Block have 4 vertices!" << std::endl;
		return -1;
	}

	auto f = [](Point2f p1, Point2f p2) -> float {return sqrt((p2.x - p1.x)*(p2.x - p1.x) + (p2.y - p1.y)*(p2.y - p1.y)); };

	float dist01 = f(vertices[1], vertices[0]);
	float dist23 = f(vertices[2], vertices[3]);
	float dist03 = f(vertices[0], vertices[3]);
	float dist12 = f(vertices[1], vertices[2]);

	this->dnb_stepX = (dist01 + dist23) / (2.0 * this->dnb_numX);
	this->dnb_stepY = (dist03 + dist12) / (2.0 * this->dnb_numY);

	return 0;
}

int BlockDC::calLineSlope()
{
	if (this->vertices.empty() || this->vertices.size() != 4)
	{
		std::cerr << "Block have 4 vertices!" << std::endl;
		return -1;
	}

	auto calk = [](Point2f p1, Point2f p2) -> float {return (p2.y - p1.y) / (p2.x - p1.x); };
	auto calb = [](Point2f p, float k) -> float {return p.y - k*p.x; };
	//check if the line is vertical, to eliminate dividing-zero bug.
	if (abs(vertices[1].x - vertices[0].x) < 1e-6)
	{
		this->topHorLine.isVertical = true;
	}
	else
	{
		float k01 = calk(vertices[1], vertices[0]), b01 = calb(vertices[0], k01);
		this->topHorLine.k = k01; this->topHorLine.b = b01;
	}
	

	if (abs(vertices[3].x - vertices[0].x) < 1e-6)
	{
		this->leftVerLine.isVertical = true;
	}
	else
	{
		float k03 = calk(vertices[3], vertices[0]), b03 = calb(vertices[0], k03);
		this->leftVerLine.k = k03; this->leftVerLine.b = b03;
	}

	return 0;
	
};


int BlockDC::calAddress(const cv::Point2i& pointOnTempl, cv::Point2f& pointOnImg )
{	
	int x = pointOnTempl.x, y = pointOnTempl.y;
	Point2f left = 1.0f * (this->dnb_numY - y) / this->dnb_numY * this->vertices[0] + 1.0f * y / this->dnb_numY * this->vertices[3];
	Point2f right = 1.0f * (this->dnb_numY - y) / this->dnb_numY * this->vertices[1] + 1.0f * y / this->dnb_numY * this->vertices[2];
	pointOnImg = 1.0f * (this->dnb_numX - x) / this->dnb_numX * left + 1.0f * x / this->dnb_numX * right;
	
	return 0;
}


int BlockDC::initBlock(const unsigned int& numX, const unsigned int& numY, const vector<Point2f>& ver)
{
	this->dnb_numX = numX;
	this->dnb_numY = numY;
	if (!this->vertices.empty())
	{
		this->vertices.clear();
	}
	this->vertices = ver;
	return this->calBlock();
}

int BlockDC::initBlock()
{
	return this->calBlock();
}
