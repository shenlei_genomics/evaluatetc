#include "BlockForAddress.h"
#include <math.h>
#define ROUND

BlockForAddress::BlockForAddress()
{
}

//int BlockForAddress::START_ADDRESS[BLOCKROW] = { -4, 176, 328, 520, 740, 980, 1220, 1440, 1632, 1784 };
//unsigned int BlockForAddress::DNBNUM[BLOCKCOL] = { 90, 76, 96, 110, 120, 120, 110, 96, 76, 89 };

BlockForAddress::~BlockForAddress()
{
	//this->m_block.~Block();
}

int BlockForAddress::initBlockForAddress()
{
	if (this->startX < 0 || this->startX > START_ADDRESS_X.size()-1 || this->startY < 0 || this->startY > START_ADDRESS_Y.size()-1)
	{
		return -2;
	}

	this->dnb_num_start_X = START_ADDRESS_X[this->startX];
	this->dnb_num_start_Y = START_ADDRESS_Y[this->startY];
	return this->m_block.initBlock();
}

std::vector<int> BlockForAddress::DNBNUMX; //= { 90, 76, 96, 110, 120, 120, 110, 96, 76, 89 };
std::vector<int> BlockForAddress::DNBNUMY; //= { 90, 76, 96, 110, 120, 120, 110, 96, 76, 89 };
std::vector<float> BlockForAddress::START_ADDRESS_X; //= { -4, 176, 328, 520, 740, 980, 1220, 1440, 1632, 1784 };
std::vector<float> BlockForAddress::START_ADDRESS_Y; //= { -4, 176, 328, 520, 740, 980, 1220, 1440, 1632, 1784 };

extern void parseStr(const string& str, vector<int>& vec);
extern int parseVec(const vector<int>& rawVec, vector<int>& dnbnum, const bool is_extend);
int BlockForAddress::initTemplate(const std::string& strDnbnumX, const std::string& strDnbnumY, const float pitchSize)
{
	DNBNUMX.clear(); DNBNUMY.clear(); START_ADDRESS_X.clear(); START_ADDRESS_Y.clear();
	vector<int> tmpVec;
	parseStr(strDnbnumX, tmpVec);
	if (parseVec(tmpVec, DNBNUMX) < 0)
	{
		return -1;
	}
	tmpVec.clear();
	parseStr(strDnbnumY, tmpVec);
	if (parseVec(tmpVec, DNBNUMY) < 0)
	{
		return -1;
	}
	
	float preValue = -2 * pitchSize;
	START_ADDRESS_X.push_back(preValue);
	for (auto it = DNBNUMX.cbegin(); it != DNBNUMX.cend() - 1; ++it)
	{
		preValue += *it * pitchSize;
		START_ADDRESS_X.push_back(preValue);
	}
	
	preValue = -2 * pitchSize;
	START_ADDRESS_Y.push_back(preValue);
	for (auto it = DNBNUMY.cbegin(); it != DNBNUMY.cend() - 1; ++it)
	{
		preValue += *it * pitchSize;
		START_ADDRESS_Y.push_back(preValue);
	}
	return 0;
}

int BlockForAddress::initTemplate(const std::string& strDnbnumX, const std::string& strDnbnumY, const float pitchSize, const bool& is_extend)
{
	DNBNUMX.clear(); DNBNUMY.clear(); START_ADDRESS_X.clear(); START_ADDRESS_Y.clear();
	vector<int> tmpVec;
	parseStr(strDnbnumX, tmpVec);
	if (parseVec(tmpVec, DNBNUMX, is_extend) < 0)
	{
		return -1;
	}
	tmpVec.clear();
	parseStr(strDnbnumY, tmpVec);
	if (parseVec(tmpVec, DNBNUMY, is_extend) < 0)
	{
		return -1;
	}

	float preValue = -2 * pitchSize;
	START_ADDRESS_X.push_back(preValue);
	for (auto it = DNBNUMX.cbegin(); it != DNBNUMX.cend() - 1; ++it)
	{
		preValue += *it * pitchSize;
		START_ADDRESS_X.push_back(preValue);
	}

	preValue = -2 * pitchSize;
	START_ADDRESS_Y.push_back(preValue);
	for (auto it = DNBNUMY.cbegin(); it != DNBNUMY.cend() - 1; ++it)
	{
		preValue += *it * pitchSize;
		START_ADDRESS_Y.push_back(preValue);
	}
	return 0;
}

int BlockForAddress::addressOnTempl(const cv::Point2f &point, cv::Point2f &pointOnTempl, float &rms)
{
	/*const auto& tmpAddressmap = this->m_block.getAddressmap();
	if (tmpAddressmap.empty() || tmpAddressmap.size() == 0)
	{
	cerr << "Error:map is empty!" << endl;
	return -1;
	}*/

	float yAddress = this->m_block.getAddress(point, this->m_block.topHorLine, this->m_block.dnb_stepY);
	float xAddress = this->m_block.getAddress(point, this->m_block.leftVerLine, this->m_block.dnb_stepX);
#ifdef ROUND
	auto round = [](float r)->int {return (r > 0.0) ? floor(r + 0.5) : ceil(r - 0.5); };
	int xAddress_int = round(xAddress), yAddress_int = round(yAddress);
	vector<Point2i> neigbors = { Point2i(xAddress_int - 1, yAddress_int - 1), \
		Point2i(xAddress_int - 1, yAddress_int),\
		Point2i(xAddress_int - 1, yAddress_int + 1), \
		Point2i(xAddress_int, yAddress_int - 1),\
		Point2i(xAddress_int, yAddress_int), \
		Point2i(xAddress_int, yAddress_int + 1), \
		Point2i(xAddress_int + 1, yAddress_int - 1), \
		Point2i(xAddress_int + 1, yAddress_int), \
		Point2i(xAddress_int + 1, yAddress_int + 1)
	};
#else
	vector<Point2i> neigbors = { Point2i(static_cast<int>(xAddress - 1), static_cast<int>(yAddress - 1)), \
		Point2i(static_cast<int>(xAddress - 1), static_cast<int>(yAddress)),\
		Point2i(static_cast<int>(xAddress - 1), static_cast<int>(yAddress + 1)), \
		Point2i(static_cast<int>(xAddress), static_cast<int>(yAddress - 1)),\
		Point2i(static_cast<int>(xAddress), static_cast<int>(yAddress)), \
		Point2i(static_cast<int>(xAddress), static_cast<int>(yAddress + 1)), \
		Point2i(static_cast<int>(xAddress + 1), static_cast<int>(yAddress - 1)), \
		Point2i(static_cast<int>(xAddress + 1), static_cast<int>(yAddress)), \
		Point2i(static_cast<int>(xAddress + 1), static_cast<int>(yAddress + 1)) };
#endif	

	//auto f = [](cv::Point2f p1, cv::Point2f p2) -> float {return sqrt((p2.x - p1.x)*(p2.x - p1.x) + (p2.y - p1.y)*(p2.y - p1.y)); };
	auto f = [](cv::Point2f p1, cv::Point2f p2) -> float {return (p2.x - p1.x)*(p2.x - p1.x) + (p2.y - p1.y)*(p2.y - p1.y); };

	rms = (numeric_limits<float>::max)();
	for (auto itn = neigbors.cbegin(); itn != neigbors.cend(); ++itn)
	{
		Point2f pointOnImg;
		this->m_block.calAddress(*itn, pointOnImg);
		float d = f(pointOnImg, point);
		if (rms > d)
		{
			pointOnTempl = cv::Point2f(BlockDC::pitchSize * itn->x + this->dnb_num_start_X, BlockDC::pitchSize * itn->y + this->dnb_num_start_Y);			
			rms = d;
		}
	}
	if (rms == (numeric_limits<float>::max)())
	{
		return -2;
	}
	return 0;
}

int BlockForAddress::addressOnTempl(const cv::Point2f &point, cv::Point2f &pointOnTempl, cv::Point2f &point_tc, float &rms)
{
	/*const auto& tmpAddressmap = this->m_block.getAddressmap();
	if (tmpAddressmap.empty() || tmpAddressmap.size() == 0)
	{
		cerr << "Error:map is empty!" << endl;
		return -1;
	}*/

	float yAddress = this->m_block.getAddress(point, this->m_block.topHorLine, this->m_block.dnb_stepY);
	float xAddress = this->m_block.getAddress(point, this->m_block.leftVerLine, this->m_block.dnb_stepX);
#ifdef ROUND
	auto round = [](float r)->int {return (r > 0.0) ? floor(r + 0.5) : ceil(r - 0.5); };
	int xAddress_int = round(xAddress), yAddress_int = round(yAddress);
	vector<Point2i> neigbors = { Point2i(xAddress_int - 1, yAddress_int - 1), \
		Point2i(xAddress_int - 1, yAddress_int),\
		Point2i(xAddress_int - 1, yAddress_int + 1), \
		Point2i(xAddress_int, yAddress_int - 1),\
		Point2i(xAddress_int, yAddress_int), \
		Point2i(xAddress_int, yAddress_int + 1), \
		Point2i(xAddress_int + 1, yAddress_int - 1), \
		Point2i(xAddress_int + 1, yAddress_int), \
		Point2i(xAddress_int + 1, yAddress_int + 1)
};
#else
	vector<Point2i> neigbors = { Point2i(static_cast<int>(xAddress - 1), static_cast<int>(yAddress - 1)), \
		Point2i(static_cast<int>(xAddress - 1), static_cast<int>(yAddress)),\
		Point2i(static_cast<int>(xAddress - 1), static_cast<int>(yAddress + 1)), \
		Point2i(static_cast<int>(xAddress), static_cast<int>(yAddress - 1)),\
		Point2i(static_cast<int>(xAddress), static_cast<int>(yAddress)), \
		Point2i(static_cast<int>(xAddress), static_cast<int>(yAddress + 1)), \
		Point2i(static_cast<int>(xAddress + 1), static_cast<int>(yAddress - 1)), \
		Point2i(static_cast<int>(xAddress + 1), static_cast<int>(yAddress)), \
		Point2i(static_cast<int>(xAddress + 1), static_cast<int>(yAddress + 1)) };
#endif	
	
	//auto f = [](cv::Point2f p1, cv::Point2f p2) -> float {return sqrt((p2.x - p1.x)*(p2.x - p1.x) + (p2.y - p1.y)*(p2.y - p1.y)); };
	auto f = [](cv::Point2f p1, cv::Point2f p2) -> float {return (p2.x - p1.x)*(p2.x - p1.x) + (p2.y - p1.y)*(p2.y - p1.y); };

	rms = (numeric_limits<float>::max)();
	for(auto itn= neigbors.cbegin(); itn != neigbors.cend(); ++itn)
	{
		Point2f pointOnImg;
		this->m_block.calAddress(*itn, pointOnImg);
		float d = f(pointOnImg, point);
		if (rms > d)
		{
			pointOnTempl = cv::Point2f(BlockDC::pitchSize * itn->x + this->dnb_num_start_X, BlockDC::pitchSize * itn->y + this->dnb_num_start_Y);
			point_tc = pointOnImg;
			rms = d;
		}
	}
	if (rms == (numeric_limits<float>::max)())
	{
		return -2;
	}
	return 0;
}
