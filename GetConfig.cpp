#include "GetConfig.h"

#include <fstream>
#include <iostream>
#include <iterator>
using namespace std;

bool IsSpace(char c)
{
	if (' ' == c || '\t' == c)
		return true;
	return false;
}

bool IsCommentChar(char c)
{
	switch (c) {
	case COMMENT_CHAR:
		return true;
	default:
		return false;
	}
}

void Trim(string & str)
{
	if (str.empty()) {
		return;
	}
	int i, start_pos, end_pos;
	for (i = 0; i < str.size(); ++i) {
		if (!IsSpace(str[i])) {
			break;
		}
	}
	if (i == str.size()) { // 全部是空白字符串
		str = "";
		return;
	}

	start_pos = i;

	for (i = str.size() - 1; i >= 0; --i) {
		if (!IsSpace(str[i])) {
			break;
		}
	}
	end_pos = i;

	str = str.substr(start_pos, end_pos - start_pos + 1);
}

bool AnalyseLine(const string & line, string & key, string & value)
{
	if (line.empty())
		return false;
	int start_pos = 0, end_pos = line.size() - 1, pos;
	if ((pos = line.find(COMMENT_CHAR)) != -1) {
		if (0 == pos) {  // 行的第一个字符就是注释字符
			return false;
		}
		end_pos = pos - 1;
	}
	string new_line = line.substr(start_pos, end_pos + 1 - start_pos);  // 预处理，删除注释部分

	if ((pos = new_line.find('=')) == -1)
		return false;  // 没有=号

	key = new_line.substr(0, pos);
	value = new_line.substr(pos + 1, end_pos + 1 - (pos + 1));

	Trim(key);
	if (key.empty()) {
		return false;
	}
	Trim(value);
	return true;
}

bool ReadConfig(const string & filename, map<string, string> & m)
{
	m.clear();
	ifstream infile(filename.c_str());
	if (!infile) {
		cout << "file open error" << endl;
		return false;
	}
	string line, key, value;
	while (getline(infile, line)) {
		if (AnalyseLine(line, key, value)) {
			m[key] = value;
		}
	}

	infile.close();
	return true;
}

void PrintConfig(const map<string, string> & m)
{
	map<string, string>::const_iterator mite = m.begin();
	for (; mite != m.end(); ++mite) {
		cout << mite->first << "=" << mite->second << endl;
	}
}

bool getParameter(const map<string,string>& m,const string& key, string& value)
{
	auto it = m.find(key);
	if (it != m.end())
	{
		value = it->second;
		return true;
	}
	else
	{
		return false;
	}
}

void parseStr(const string& str, vector<int>& vec)
{
	std::istringstream iss(str);
	while (iss)
	{
		std::string sub;
		iss >> sub;
		if (sub.size() > 0)
		{
			vec.push_back(static_cast<int>(std::stoi(sub)));
		}
	}
}

int parseVec(const vector<int>& rawVec, vector<int>& dnbnum, const bool is_extend)
{
	if (rawVec.empty() || rawVec.size() < 1)
	{
		return -1;
	}
	if (is_extend)
	{
		unsigned int last = rawVec[rawVec.size() - 1];
		unsigned int firstElement = last / 2 + 2;
		unsigned int lastElement = last / 2 + 1;
		copy(rawVec.cbegin(), rawVec.cend() - 1, std::back_inserter(dnbnum));
		dnbnum.insert(dnbnum.cbegin(), firstElement);
		dnbnum.push_back(lastElement);
	}
	else
	{
		dnbnum=rawVec;
	}
	
	return 0;
}
