#include "Tools.h"
#include "GetConfig.h"

using namespace cv;

using namespace std;
int ReadIni(const std::string& fn, cv::Mat& cameraMatrix, cv::Mat& distCoeffs)
{
	cameraMatrix = Mat::eye(3, 3, CV_64F);
	distCoeffs = Mat::zeros(8, 1, CV_64F);
	
	std::ifstream fs(fn, std::fstream::in);
	if (!fs)
	{
		std::cerr << "Read INI file " << fn << " error!" << std::endl;
		return -1;
	}

	double u0, v0, fx, fy, k1, k2, k3, p1, p2;
	map<string, double> ini_map;	

	string line;
	getline(fs, line);
	int line_index = 0;
	while (line_index < 9)
	{
		getline(fs, line);
		string key, value;
		AnalyseLine(line, key, value);
		std::stringstream ss(value);
		double tmp;
		ss >> tmp;
		ini_map[key] = tmp;
		line_index++;
	}
	fs.close();

	cameraMatrix.at<double>(0, 2) = ini_map["u0"];
	cameraMatrix.at<double>(1, 2) = ini_map["v0"];
	cameraMatrix.at<double>(0, 0) = ini_map["fx"];
	cameraMatrix.at<double>(1, 1) = ini_map["fy"];
	distCoeffs.at<double>(0, 0) = ini_map["k1"];
	distCoeffs.at<double>(1, 0) = ini_map["k2"];
	distCoeffs.at<double>(4, 0) = ini_map["k3"];
	distCoeffs.at<double>(2, 0) = ini_map["p1"];
	distCoeffs.at<double>(3, 0) = ini_map["p2"];

}