#pragma once
#include <sstream>
#include <vector>
#include <io.h>
#include "opencv2\core\core.hpp"
#include <fstream>
#include <iostream>
#include  <stdio.h>
#include  <stdlib.h>
#include "BlockForAddress.h"

const float RMSTH = 1.0f;
const unsigned int COUNTTH = 200;
const float BLOCKRMSTH = 0.09f;
const size_t CAL_TIF_COUNT = 5;
//#include <limits>
struct IMGINFO
{
	std::string imgFn;	
	float rms4sel;//rms for select images which tc is accurate	
	float rms4comp;//rms for compare of Even's TC and calibrated TC
	int n4sel;// goodpoints num for select image

	//points for calibration on img, only use good block's(judged by block's rms) good points
	std::vector<cv::Point2f> points4cal_img;
	std::vector<cv::Point2f> points4cal_templ;
	

	//points for compare on img, only use outside block for compare.
	std::vector<cv::Point2f> points4comp_img;
	std::vector<cv::Point2f> points4comp_templ;
	std::vector<cv::Point2f> points4comp_tc;

	IMGINFO(const std::string& imgfn,  const float r, const float part_r, const int n) :imgFn(imgfn), rms4sel(r), rms4comp(part_r), n4sel(n) {};
	IMGINFO() :imgFn(""), rms4sel(0.0f), rms4comp(0.0f), n4sel(0) {};
	IMGINFO(const std::string& imgfn, const float r, const float part_r, const int n,const std::vector<cv::Point2f>& pi, const std::vector<cv::Point2f>& pt, \
		const std::vector<cv::Point2f>& pi4c, const std::vector<cv::Point2f>& pt4c) : \
		imgFn(imgfn), rms4sel(r),rms4comp(part_r), n4sel(n), points4cal_img(pi), points4cal_templ(pt), points4comp_img(pi4c), points4comp_templ(pt4c) {};
	~IMGINFO() 
	{
		points4cal_img.clear();
		points4cal_templ.clear();
		points4comp_img.clear();
		points4comp_templ.clear();
		points4comp_tc.clear();
	}
};

int getFiles(const std::string& path, const std::string& ext, std::vector<std::string>& files);

//int initBlocks(std::vector<Block> &blocks, const std::vector<cv::Point2f> &trackcross, const std::vector<int> &dnbnumX, const std::vector<int> &dnbnumY);

//int ProcessOneTiff(const cv::Mat &goodPointsMask, const cv::Mat(&centroid)[2], std::vector<Block> &blocks, const unsigned  int BLOCK_SIZE, float& err);

//int runOneTiff(const std::string &imgFn, std::vector<IMGINFO>& errs);

//int selGoodImgs(const std::string& tifpath, std::vector<IMGINFO>& imginfolist, TrackDetector& detector, cv::Size& imgSize, const std::size_t& TIF_COUNT = 5);

int initBlocksForAddress(std::vector<BlockForAddress> &blocks, const std::vector<Point2f> &trackcross, const size_t& BLOCKROW, const size_t& BLOCKCOL);
int ProcessOneBlock(const cv::Mat &goodPointsMask, const cv::Mat(&centroid)[2], const bool if_outside_blk, BlockForAddress &block, IMGINFO& info);