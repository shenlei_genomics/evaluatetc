#pragma once
#include "Tools.h"
#include "GetConfig.h"
#include "BlockDC.h"
#include "BlockForAddress.h"
#include <vector>
#include <map>
#include <memory>
#include <iostream>
#include <io.h>
#include <iterator>
#include <algorithm>
#include "SelectImages.h"
#include "SelectGoodPoints.h"
using namespace std;
using namespace cv;

int initParameters(const string& configFn, std::map<std::string, std::string>& config_map, string& strDnbnumX, string& strDnbnumY)
{
	if (!ReadConfig(configFn, config_map))
	{
		std::cerr << "Read config file:" << configFn << " error!" << std::endl;
		return -1;
	}
	string strChippitch, strImgscale, str_extend;
	bool is_extend = false;
	bool ret = getParameter(config_map, "modparam.basecall.ImageTrackPattern_gridSizeX[]", strDnbnumX) && \
		getParameter(config_map, "modparam.basecall.ImageTrackPattern_gridSizeY[]", strDnbnumY) && \
		getParameter(config_map, "modparam.basecall.chip_pitch", strChippitch) && \
		getParameter(config_map, "modparam.basecall.img_scale", strImgscale)&&
	    getParameter(config_map, "modparam.basecall.trackExtend", str_extend);
	is_extend = (str_extend == "true" ? true : false);

	if (!ret)
	{
		return -1;
	}
	if (BlockDC::initPitchsize(strChippitch, strImgscale) < 0)
	{
		return -1;
	}
	if (BlockForAddress::initTemplate(strDnbnumX, strDnbnumY, BlockDC::pitchSize, is_extend) < 0)
	{
		return -1;
	}
	return 0;
}

int calRMS(const string& imgFn, const vector<Point2f>& trackcross,IMGINFO& info)
{
	Mat goodPointsMask;
	Mat centroid[2];

	const size_t BLOCKROW = BlockForAddress::getBlockrow();
	const size_t BLOCKCOL = BlockForAddress::getBlockcol();
	vector<BlockForAddress> blocks; blocks.reserve(BLOCKROW*BLOCKCOL);

	if (findGoodPoints(imgFn, goodPointsMask, centroid))
	{
		cerr << "Find good points error! image:" << imgFn << endl;
		return -1;
	}

	if (initBlocksForAddress(blocks, trackcross, BLOCKROW, BLOCKCOL) < 0)
	{
		cerr << "Init Block error! Image:" << imgFn << endl;
		return -1;
	}
	for (size_t block_id = 0; block_id < BLOCKROW*BLOCKCOL; block_id++)
	{
		if (ProcessOneBlock(goodPointsMask, centroid, false, blocks[block_id], info) < 0)
		{
			cerr << "Process block " << block_id << " error!" << endl;
			return -1;
		}
	}
	if (info.n4sel == 0)
	{
		info.rms4sel = numeric_limits<float>::max();
	}
	else
	{
		info.rms4sel = (info.rms4sel + 0.0f) / info.n4sel;		
	}
	cout << "RMS:" << info.rms4sel << endl;
	return 0;
}

void printUsage(char* prog)
{
	cout << "Usage:" << prog << " img_path tc_fn [setting.config]"
		<< "\nThis is a tool to calculate RMS of trackcross. RMS will be print on cmd screen."
		<< endl;
}

int readTrackcross(const string& tc_fn, vector<vector<Point2f>>& trackcorss)
{
	trackcorss.clear();
	ifstream input(tc_fn);
	vector<Point2f> tc;
	if (input)
	{
		string line, dump;
		while (getline(input, line))
		{
			if (line.length() < 2)
			{
				continue;
			}
			if (line.substr(0,1) == "#")
			{
				if (tc.size() > 0)
				{
					trackcorss.push_back(tc);
					tc.clear();
				}
				continue;
			}
			float x, y;
			istringstream stream(line);
			stream >> x >> dump;
			if (dump.substr(0,1) == ",")
			{
				istringstream iss(dump.substr(1, dump.length() - 1));
				iss >> y;
			}
			else
			{
				istringstream iss(dump);
				iss >> y;
			}
			tc.push_back(Point2f(x, y));
		}
		trackcorss.push_back(tc);
		input.close();
	}
	else
	{
		input.close();
		return -1;
	}
	return 0;
}

int main(int argc, char* argv[])
{
	if (argc < 3)
	{
		printUsage(argv[0]);
		return -1;
	}
	string tc_fn, setting_fn, img_path;
	if (argc == 3)
	{
		img_path = argv[1];
		tc_fn = argv[2];
		setting_fn = "./settings.config";
	}
	if (argc > 3)
	{
		img_path = argv[1];
		tc_fn = argv[2];
		setting_fn = argv[3];
	}
	if (_access(img_path.c_str(), 0) < 0)
	{
		cerr << "Image path: " << img_path << " not exist!" << endl;
		return -1;
	}
	if (_access(tc_fn.c_str(), 0) < 0 )
	{
		cerr << "Trackcross file: "<<tc_fn << " not exist!" << endl;
		return -1;
	}
	if (_access(setting_fn.c_str(), 0) < 0)
	{
		cerr << "Settings config file: "<<setting_fn << " not exist!" << endl;
		return -1;
	}
	vector<vector<Point2f>> trackcross;
	int ret = 0;	
	std::map<std::string, std::string> config_map; string strDnbnumX, strDnbnumY;
	ret = readTrackcross(tc_fn, trackcross);
	if (ret < 0 || trackcross.size() < 1)
	{
		return ret;
	}
	ret = initParameters(setting_fn, config_map, strDnbnumX, strDnbnumY);
	assert(trackcross[0].size() == (BlockForAddress::getBlockrow()+1) * (BlockForAddress::getBlockcol() +1));
	if (ret < 0)
	{
		return ret;
	}
	if (ret < 0)
	{
		return ret;
	}
	
	vector<string> img_fns;
	ret = getFiles(img_path, ".tif", img_fns);
	if (ret < 0)
	{
		return ret;
	}
	assert(img_fns.size() == trackcross.size());
	vector<string>::const_iterator itfb = img_fns.cbegin();
	vector<string>::const_iterator itfe = img_fns.cend();
	vector<vector<Point2f>>::const_iterator ittcb = trackcross.cbegin();	
	vector<string>::const_iterator itf = itfb;
	vector<vector<Point2f>>::const_iterator ittc = ittcb;
	for (; itf != itfe; ++itf, ++ittc)
	{
		IMGINFO info;
		calRMS(*itf, *ittc, info);
	}
	system("Pause");
	
}