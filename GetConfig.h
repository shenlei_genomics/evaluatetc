/****************************************************************************
*   作者:  jasitzhang(张涛)
*   日期:  2011-10-2
*   目的:  读取配置文件的信息，以map的形式存入
*   要求:  配置文件的格式，以#作为行注释，配置的形式是key = value，中间可有空格，也可没有空格
*****************************************************************************/
#pragma once
#include <string>
#include <map>
#include <sstream>
#include <vector>
#include <algorithm>
using namespace std;

const char COMMENT_CHAR = '#';

bool ReadConfig(const string & filename, map<string, string> & m);
void PrintConfig(const map<string, string> & m);
bool getParameter(const map<string, string>& m, const string& key, string& value);
void parseStr(const string& str, vector<int>& vec);
int parseVec(const vector<int>& rawVec, vector<int>& dnbnum, const bool is_extend = true);
bool AnalyseLine(const string & line, string & key, string & value);