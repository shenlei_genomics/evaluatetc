#pragma once
#include <string>
#include <opencv2\core.hpp>
#include <iostream>
#include <fstream>
#include <map>

int ReadIni(const std::string& fn, cv::Mat& cameraMatrix, cv::Mat& distCoeffs);