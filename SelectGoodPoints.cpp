#include "SelectGoodPoints.h"
#include <iostream>
#include <time.h> 
#include <ostream>
//#define _DEBUG

/*3*3 cell peak point judgment: with background as threshold*/
//float getThreshold(const Mat& src, const int thIdx)
//{
//	Mat sortedSrc;
//	src.copyTo(sortedSrc);
//	MatIterator_<float> it = sortedSrc.begin<float>(), it_end = sortedSrc.end<float>();
//	nth_element(it, it + thIdx, it_end);
//	const float* curRowPtr = sortedSrc.ptr<float>(thIdx / sortedSrc.cols);
//	float thresh = curRowPtr[thIdx%sortedSrc.cols];
//	sortedSrc.release();
//	return thresh;
//}

float getThreshold(vector<float>& v, const float& thPercent)
{
	size_t thIdx = (1 - thPercent)*v.size();
	nth_element(v.begin(), v.begin() + thIdx, v.end(), [](const float e1, const float e2)->bool {return e1 > e2; });	
	return v[thIdx];
}

void findPeakPtsVals(const cv::Mat& img, vector<PeakInfo>& peak, const PARAM& param)
{
	vector<float> focus;
	vector<float> peakInts;
	vector<PeakInfo> candidate;
	const size_t Y_UP_LIMIT = img.rows - 1
		, X_UP_LIMIT = img.cols - 1;
	for (size_t y = 1; y < Y_UP_LIMIT; ++y)
	{
		size_t x = 1;
		while (x < X_UP_LIMIT)
		{	
			
			if (judgePeakPoint(img, cv::Point(static_cast<int>(x), static_cast<int>(y))))
			{
				float s1 = img.at<float>(y - 1, x - 1) + img.at<float>(y - 1, x) + img.at<float>(y - 1, x + 1);
				float s2 = img.at<float>(y, x - 1) + img.at<float>(y, x) + img.at<float>(y, x + 1);
				float s3 = img.at<float>(y + 1, x - 1) + img.at<float>(y + 1, x) + img.at<float>(y + 1, x + 1);
				float pi = img.at<float>(y, x);
				float f = pi / (s1 + s2 + s3);
				focus.push_back((f));
				peakInts.push_back(pi);
				float cen_y = (s3 - s1) / (s1 + s2 + s3);
				if (cen_y > param.centroidUpTh || cen_y < param.centroidLowTh)
				{
					x += 2;
					continue;
				}
				float c1 = img.at<float>(y - 1, x - 1) + img.at<float>(y, x - 1) + img.at<float>(y + 1, x - 1);
				float c3 = img.at<float>(y - 1, x + 1) + img.at<float>(y, x + 1) + img.at<float>(y + 1, x + 1);
				float cen_x = (c3 - c1) / (s1 + s2 + s3);
				if (cen_x > param.centroidUpTh || cen_x < param.centroidLowTh)
				{
					x += 2;
					continue;
				}
				candidate.emplace_back(Point(static_cast<int>(x), static_cast<int>(y)), f, pi, cen_x, cen_y);
				x += 2;
			}
			else
				x += 1;			
					
		}
	}
	float focus_th = getThreshold(focus, param.focusScorePercentTh);
	float int_th = getThreshold(peakInts, param.intensPercentTh);
	peak.clear();	
#ifdef  _DEBUG
	std::sort(candidate.begin(), candidate.end(), [](const PeakInfo& p1, const PeakInfo& p2)->bool {return p1.intens > p2.intens; });
	std::sort(candidate.begin(), candidate.end(), [](const PeakInfo& p1, const PeakInfo& p2)->bool {return p1.focus > p2.focus; });
#endif //  _DEBUG

	for (vector<PeakInfo>::const_iterator it = candidate.cbegin(); it != candidate.cend(); ++it)
	{
		
		if (it->focus > focus_th && it->intens > int_th)
		{
			peak.push_back(*it);
		}
	}
#ifdef _DEBUG
	ofstream fs("./goodpoints.txt");
	if (fs)
	{
		for (vector<PeakInfo>::const_iterator it = peak.cbegin(); it != peak.cend(); ++it)
		{
			fs << it->loc.x <<"\t" << it->loc.y << "\n";			
		}
	}
	fs.close();
	/*std::sort(peak.begin(), peak.end(), [](const PeakInfo& p1, const PeakInfo& p2)->bool {return p1.intens > p2.intens; });
	ofstream fs1("./goodpoints_high_intens.txt");
	if (fs1)
	{
		for (vector<PeakInfo>::const_iterator it = peak.cbegin(); it != peak.cbegin()+200; ++it)
		{
			fs1 << it->loc.x << "\t" << it->loc.y << "\n";
		}
	}
	fs1.close();
	std::sort(peak.begin(), peak.end(), [](const PeakInfo& p1, const PeakInfo& p2)->bool {return p1.focus > p2.focus; });
	ofstream fs2("./goodpoints_high_focus.txt");
	if (fs2)
	{
		for (vector<PeakInfo>::const_iterator it = peak.cbegin(); it != peak.cbegin() + 200; ++it)
		{
			fs2 << it->loc.x << "\t" << it->loc.y << "\n";
		}
	}
	fs2.close();*/
#endif
	
}


int findGoodPoints(const string& imgFn, const string& gpMaskFn, const PARAM& param )
{
	Mat goodPointsMask;
	Mat centroid[2];
	int ret = findGoodPoints(imgFn, goodPointsMask, centroid, param);
	if (imwrite(gpMaskFn, goodPointsMask))
	{
		return 0;
	}
	else
		return -2;
}

int findGoodPoints(const string& imgFn, Mat& goodPointsMask, Mat (&centroidMat)[2], const PARAM& param)
{
	//clock_t cstart, cends;
	//cstart = clock();
	Mat img = imread(imgFn, CV_16UC1);
	if (img.empty())
	{
		return -1;
	}
	img.convertTo(img, CV_32FC1);

	vector<PeakInfo> peak;
	findPeakPtsVals(img, peak, param);

	goodPointsMask=cv::Mat::zeros(img.size(), CV_8U);
	centroidMat[0]=cv::Mat::zeros(img.size(), CV_32FC1);
	centroidMat[1]= cv::Mat::zeros(img.size(), CV_32FC1);

	for (vector<PeakInfo>::const_iterator it = peak.cbegin(); it != peak.cend(); ++it)
	{
		goodPointsMask.at<unsigned char>(it->loc.y, it->loc.x) = 255;
		centroidMat[0].at<float>(it->loc.y, it->loc.x) = it->cen_x;
		centroidMat[1].at<float>(it->loc.y, it->loc.x) = it->cen_y;
	}

	//cends = clock();
	//cout << "findGoodPoints running time��" << cends - cstart << endl;
	return 0;
}